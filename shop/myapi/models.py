from django.db import models
from django.contrib.auth import get_user_model






class Product(models.Model):
    product_ID = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    category = models.CharField(max_length=500,default='Null')
    price = models.FloatField()
    

    def __str__(self):
        return self.name

class ProductCategory(models.Model):
    category_ID = models.AutoField(primary_key=True)
    products = models.ManyToManyField(Product)
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name

class Order(models.Model):
    order_ID = models.AutoField(primary_key=True)
    user = models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
    
    

    def __str__(self):
        return str(self.order_ID)

class OrderDetails(models.Model):
    details_ID = models.AutoField(primary_key=True)
    order = models.ForeignKey(Order,on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    name = models.CharField(max_length=500)
    price = models.FloatField()
    quantity = models.IntegerField()

    def __str__(self):
        return str(self.order)