from django.shortcuts import render
from rest_framework import viewsets, permissions 
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from .serializers import  UserSerializer, ProductSerializer, ProductCategorySerializer, OrderSerializer, OrderDetailsSerializer
from django.contrib.auth.models import User
from .models  import Product, ProductCategory, Order, OrderDetails





class UserListViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by('name')
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticated]
    
class ProductCategoryViewSet(viewsets.ModelViewSet):
    queryset = ProductCategory.objects.all().order_by('name')
    serializer_class = ProductCategorySerializer
    permission_classes = [IsAdminUser]
   
class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by('user')
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]
class OrderDetailsViewSet(viewsets.ModelViewSet):
    queryset = OrderDetails.objects.all().order_by('order')
    serializer_class = OrderDetailsSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]


