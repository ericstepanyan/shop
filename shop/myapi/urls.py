from django.urls import include, path
from rest_framework import routers
from . import views





router = routers.DefaultRouter()
router.register(r'users', views.UserListViewSet)
router.register(r'products', views.ProductViewSet)
router.register(r'categories', views.ProductCategoryViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'orderdetails', views.OrderDetailsViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]